"""
	This is the main file that runs everything
"""

from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, send_from_directory
import sqlite3
import string
import random
import time

# configuration

#DATABASE = '/tmp/flaskr.db'
DEBUG = True
SECRET_KEY = 'iuhg7843tyeiudhi3478diuhr8t3yt98gjoireut3tuwpoi'
dev = True


app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

# Session variables
#session["logged_in"] is a boolean determine login status
#session["Username"] is a String that store the Username of The username
#session["currName"] is a String that contain the User's name
#session["position"] contains user's position.
#						Must be "Salesperson", "Manager", "Director", or "Member"
# routing
@app.route("/")
def home():
	try:
		session["logged_in"]
	except:
		session["logged_in"] = False

	print str(session["logged_in"])
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT barcode, itemname, category, quantity,
	                         supplier, unitprice, saleflag, rating
	                         FROM stock WHERE saleflag = "true" and quantity > 0''')
	all_rows = cursor.fetchall()

	all_items = []
	for row in all_rows:
		img_path = ""
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (row[0],))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
			
		try:
			width = all_rows[0][1]
		except:
			width = 70
		
		all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
			'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'img_path': img_path, 'img_width': width,
			'rating': row[7]})
	
		
	
	db.close()
	return render_template('home.html', items = all_items)


@app.route("/logout")
def logout():
	session["logged_in"] = False
	session["username"] = ""
	session["currName"] = ""
	session["position"] = ""
	return redirect(url_for('home'))
	

@app.route("/login", methods=["POST"])
def login():
	username = request.form['Username']
	password = request.form['Password']

	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	cursor.execute('''select * from User where User.Username = ? AND User.Password = ?  
		AND User.Position = 'Member' ''', (username,password,))

	all_rows = cursor.fetchall()



	if (len(all_rows) == 1):
		session["logged_in"] = True
		session["username"] = username
		session["currName"] = all_rows[0][2]
		session["position"] = "Member"
                return redirect(url_for('home'))
	else:
		flash("Username or Password Incorect")

	return redirect(url_for('home'))
	

@app.route("/staff-login", methods=["POST", "GET"])
def staff_login():
	try:
		if session['logged_in']:
			if session['position'] == "Salesperson":
				return redirect(url_for('sales_person'))
			elif session['position'] == "Director":
				return redirect(url_for('director'))
			elif session['position'] == "Manager":
				return redirect(url_for('manager'))
	except:
		pass
		
	if request.method == "POST":
		position = request.form['position']
		username = request.form['username']
		password = request.form['password']

		db = sqlite3.connect('Database.db')
		cursor = db.cursor()
		
		cursor.execute('''select username, password, name, position from User where User.Username = ? 
			AND User.Password = ? ''', (username,password,))
		
		db_user = cursor.fetchall()
		
		if (len(db_user) == 1):
			if db_user[0][3] == position:
				session["logged_in"] = True
				session["username"] = username
				session["currName"] = db_user[0][2]
				session["position"] = position
			else:
				flash("You are not a " + position)
				return render_template('staff_login.html')
				
		else:
			flash("Username or Password Incorect")
			return render_template('staff_login.html')
			
		
		if position == "Salesperson":
			return redirect(url_for('sales_person'))
		elif position == "Director":
			return redirect(url_for('director'))
		elif position == "Manager":
			return redirect(url_for('manager'))
	
	return render_template('staff_login.html')

# users
@app.route("/member")
def member():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	return redirect(url_for("member_cart"))


@app.route("/sales-person")
def sales_person():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        return redirect(url_for("accept_order"))


@app.route("/director")
def director():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        return redirect(url_for('promote_demote'))


@app.route("/manager")
def manager():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        return redirect(url_for('mg_browse_items'))
	

# other stuff users can do
@app.route("/register-member")
def register_member():
	return render_template('register_member.html')


@app.route("/submit-reg", methods=["POST"])
def submit_reg():
	db = sqlite3.connect("Database.db")
	cursor = db.cursor()

	name = request.form['name']
	username = request.form['username']
	password = request.form['password']
	email = request.form['email']

	issuccRegisterd = False;
	try:
		cursor.execute('''INSERT INTO User(Username, Password, Name, Email, Position) 
			VALUES(?,?,?,?,'Member') ''', (username,password,name,email,))
		db.commit()
		issuccRegisterd = True;

	except sqlite3.IntegrityError:
		issuccRegisterd = False;
		cursor = db.cursor()
		cursor.execute('''select username from User where User.Username = ?  ''', (username,))
		all_rows = cursor.fetchall();


		print('Record already exists')

	finally: 
		db.close()

	if (not issuccRegisterd):
		if (len(all_rows) != 0):
			flash("Your username has been taken")
		else:
			flash("Please make sure all slot are fill, password > 6.")
		return render_template('register_member.html')
	
	flash("Congraz, you are a member!!!!")	
	session['logged_in'] = True
	session['username'] = username
	session['currName'] = name
	session['position'] = 'Member'
	return redirect(url_for('home'))


@app.route("/sales-person/restock", methods = ["POST"])
def sp_restock():
	quantity = int(request.form['quantity'])
	barcode = request.form['barcode']
	print quantity
	print barcode 

	db = sqlite3.connect("Database.db")
	cursor = db.cursor()
	cursor.execute(''' select quantity from Stock where Stock.Barcode = ? ''', (barcode,))
	classquant = cursor.fetchone()
	quant = classquant[0]
	finalquant = quant + quantity
	cursor.execute(''' UPDATE Stock set Quantity = ? where Stock.Barcode = ?  ''',(finalquant, barcode))
	
	db.commit()
	db.close()


	return redirect(url_for('sp_stock_items'))


@app.route("/sales-person/restock-items")
def sp_stock_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	# also "search" and "restock"
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT barcode, itemname, category, quantity, supplier, unitprice, saleflag, rating FROM stock''')
	all_rows = cursor.fetchall()

	all_items = []
	for row in all_rows:
         cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (row[0],))
         all_rows = cursor.fetchall()
         
         try:
         	img_path = all_rows[0][0]
         except:
         	img_path = 'images/no_img.svg'
         	
         try:
         	width = all_rows[0][1]
         except:
         	width = 70
         	
         all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
		            'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'img_path': img_path, 'img_width': width,
		            'rating': row[7]})
	
	db.close()
	return render_template('sp_restock_items.html', items = all_items)


@app.route("/sales-person/accept-order")
def accept_order():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	cursor.execute('''SELECT ordernum FROM rejectedOrders''')
	all_rows = cursor.fetchall()
	rejected = []
	for row in all_rows:
		rejected.append(row[0])
	
	cursor.execute('''SELECT ordernum FROM accepts''')
	all_rows = cursor.fetchall()
	accepted = []
	for row in all_rows:
		accepted.append(row[0])
	
	cursor.execute('''SELECT ordernum, tprice, quantity, odata FROM orders''')
	all_rows = cursor.fetchall()

	all_orders = []
	for row in all_rows:
		ordernum = row[0]
		if ordernum in accepted or ordernum in rejected:
			continue
			
		temp_username = ""
		cursor.execute('''SELECT meusername FROM checkout WHERE ordernum = ? ''', (row[0],))
		temp_users = cursor.fetchall()
		try:
			temp_username = temp_users[0][0]
		except:
			print "Error finding user for order"	
		
		cursor.execute('''SELECT barcode FROM contains WHERE ordernum = ? ''', (row[0],))
		temp_order = cursor.fetchall()
		try:
			barcode = temp_order[0][0]
		except:
			print "Error finding user for order"
			barcode = 0
		
		cursor.execute('''SELECT itemname, rating FROM stock WHERE barcode = ? ''', (barcode,))
		temp_items = cursor.fetchall()
		try:
			temp_itemname = temp_items[0][0]
			rating = temp_items[0][1]
		except:
			print "Error finding item for order"
			temp_itemname = ""
			rating = 0
		
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (barcode,))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
			
		try:
			width = all_rows[0][1]
		except:
			width = 70
			
		all_orders.append({'ordernum': ordernum, 'barcode' : barcode, 'price': row[1], 'quantity': row[2], \
                    'date': row[3], 'username': temp_username, 'itemname': temp_itemname, 'img_path': img_path,
                    'img_width': width, 'rating': rating})
	db.close()
	return render_template('accept_order.html', orders = all_orders)


@app.route("/sales-person/accept-order-update", methods=['POST'])
def accept_order_update():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	ordernum = request.form['ordernum']
	accept = request.form['accept']
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	if accept == "true":
		flash("order accepted")
		cursor.execute('''INSERT INTO accepts (spusername,  ordernum, adate)
                       VALUES (?,?,?)''',(session['username'], ordernum, time.strftime("%d/%m/%Y")))
                        
	elif accept == "false":
		flash("order rejected")
		cursor.execute('''INSERT INTO RejectedOrders (ordernum)
                       VALUES (?)''',(ordernum,))
	
	db.commit()
	db.close()
	
	return redirect(url_for('accept_order'))

	
@app.route("/sales-person/settings")
def sp_settings():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT username, password, name, email, position FROM user 
						WHERE position="Salesperson"
						AND username = ?''', (session['username'],))
						
	all_rows = cursor.fetchall()
	current_user = {}
	for row in all_rows:
		current_user = {'username':row[0], 'password': row[1], 'name': row[2], \
								'email': row[3], 'position': row[4]}
	
	db.close()
	
	return render_template('sp_settings.html', user = current_user)


@app.route("/sales-person/update_settings", methods=['POST'])
def sp_update_settings():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	username = request.form['username']
	name = request.form['name']
	password = request.form['password']
	email = request.form['email']
	
	
	if len(password) < 7:
		flash("Error: password must be at least 7 characters")
		return redirect(url_for('sp_settings'))
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''UPDATE user SET name = ?, password = ?, email = ?
													WHERE username = ?
													AND position="Salesperson" ''',
											(name, password, email, username,))
											
	session['currName'] = name
	
	db.commit()
	db.close()
	flash("Settings updated")
	return redirect(url_for('sp_settings'))



@app.route("/manager/browse-items")
def mg_browse_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	# also "search" and "change price"
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT barcode, itemname, category, quantity, supplier, unitprice, saleflag, rating FROM stock''')
	all_rows = cursor.fetchall()
	
	all_items = []
	for row in all_rows:
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (row[0],))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
			
		try:
			width = all_rows[0][1]
		except:
			width = 70
		
		all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
		            'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'img_path': img_path, 'img_width': width,
		            'rating': row[7]})
		
	db.close()
	return render_template('mg_browse_items.html', items = all_items)


@app.route("/manager/update-items", methods=['POST'])
def mg_update_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	# also "search" and "change price"
	
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        barcode = request.form['barcode']
        new_price = request.form['new_price']
        
        cursor.execute('''UPDATE stock SET unitprice = ? WHERE barcode = ? ''', (new_price, barcode))
        flash("Price Changed to $" + new_price)
                
        db.commit()
        db.close
        
        return redirect(url_for('mg_browse_items'))



@app.route("/director/browse-items")
def dt_browse_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	# also "search" and "control available items"
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT barcode, itemname, category, quantity, supplier, unitprice, saleflag, rating FROM stock''')
	all_rows = cursor.fetchall()
	
	all_items = []
	for row in all_rows:
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (row[0],))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
				
		try:
			width = all_rows[0][1]
		except:
			width = 70
			
		all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
			            'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'img_path': img_path, 'img_width': width,
			            'rating': row[7] })
	db.close()
	return render_template('dt_browse_items.html', items = all_items)


@app.route("/search", methods=["POST"])
def search_items():

	search_text = request.form["search"]

	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT barcode, itemname, category, quantity,
	                         supplier, unitprice, saleflag, rating
	                         FROM stock
	                         WHERE saleflag = "true"
                                       and quantity > 0
	                               		and (itemname LIKE '%'''+search_text+'''%'
	                               		or supplier LIKE '%'''+search_text+'''%'
	                               		or category LIKE '%'''+search_text+'''%') ''')
	all_rows = cursor.fetchall()
	
	all_items = []
	for row in all_rows:
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (row[0],))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
			
		try:
			width = all_rows[0][1]
		except:
			width = 70
		
		all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
		            'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'img_path': img_path, 'img_width': width,
		            'rating': row[7]})
		db.close()
	return render_template('home.html', items = all_items)


@app.route("/member/cart")
def member_cart():
	try:
		if not session['logged_in']:
			return redirect(url_for("home"))
	except:
		return redirect(url_for("home"))
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	user = session['username']
	cursor.execute('''SELECT meusername, barcode, cart.quantity,
	                         itemname, stock.quantity, supplier, unitprice
	                  FROM cart JOIN stock using (barcode)
	                  WHERE meusername = ?''', (user,))
	all_rows = cursor.fetchall()
	db.close()

	all_items = []
	for row in all_rows:
                all_items.append({'meusername': row[0], 'barcode': row[1],
                                  'cart.quantity' : row[2], 'itemname': row[3],
                                  'stock.quantity' : row[4], 'supplier' : row[5],
                                  'price': row[6]
                                 })
	return render_template('member_cart.html', items = all_items)

	
@app.route("/member/settings")
def member_setting():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT username, password, name, email, position FROM user 
						WHERE position="Member"
						AND username = ?''', (session['username'],))
						
	all_rows = cursor.fetchall()
	current_user = {}
	for row in all_rows:
		current_user = {'username':row[0], 'password': row[1], 'name': row[2], \
								'email': row[3], 'position': row[4]}
	
	db.close()
	
	return render_template('member_setting.html', user = current_user)


@app.route("/member/update_settings", methods=['POST'])
def member_update_settings():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	username = request.form['username']
	name = request.form['name']
	password = request.form['password']
	email = request.form['email']
	
	
	if len(password) < 7:
		flash("Error: password must be at least 7 characters")
		return redirect(url_for('member_setting'))
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''UPDATE user SET name = ?, password = ?, email = ?
													WHERE username = ?
													AND position="Member" ''',
											(name, password, email, username,))
											
	session['currName'] = name
	
	db.commit()
	db.close()
	flash("Settings updated")
	return redirect(url_for('member_setting'))
	

@app.route("/checkout")
def checkout():
        """
        try:
                if not session['logged_in']:
                        return redirect(url_for("staff_login"))
        except:
                return redirect(url_for("staff_login"))
        """
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
	user = session['username']

	all_ordernums = cursor.execute('''SELECT ordernum FROM orders''')
        max_ordernum = 0
        for ordernum in all_ordernums:
            if ordernum[0] > max_ordernum:
                max_ordernum = ordernum[0]

        new_ordernum = max_ordernum + 1

	cursor.execute(''' 	SELECT quantity, barcode
						FROM cart
						WHERE meusername = ? ''', (user,))
	total_quantity = 0
	total_price = 0

	all_rows = cursor.fetchall()
	for row in all_rows:
		print row[1]
		cursor.execute(''' SELECT unitprice FROM stock WHERE barcode = ? ''', (str(row[1]),))
		all_prices = cursor.fetchall()

		total_quantity += row[0]
		total_price += all_prices[0][0]

                barcode = row[1]

		cursor.execute(''' INSERT into contains (ordernum, barcode)
		                   VALUES (?,?)''', (new_ordernum,barcode))

	cursor.execute('''INSERT into orders (ordernum, tprice, quantity, odata)
	                  VALUES (?,?,?,date('now'))''', (new_ordernum, total_price, total_quantity))

	cursor.execute('''INSERT into checkout (meusername, ordernum)
	                  VALUES (?,?)''', (session['username'], new_ordernum))

        db.commit()
	cursor.execute('''DELETE FROM cart
	                  WHERE meusername = ?''', (session['username'],))

        cursor.execute('''SELECT barcode, itemname, category, quantity,
                                 supplier, unitprice, saleflag
                                 FROM stock WHERE saleflag = "true"''')
        all_rows = cursor.fetchall()

        db.commit()
        all_items = []
        for row in all_rows:
                all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
                        'supplier': row[4], 'price': row[5], 'saleflag': row[6]})

        flash("Your order will be processed.")
        return redirect(url_for('home'))


@app.route("/update-browse", methods=["POST"])
def update_browse():
        """
        try:
                if not session['logged_in']:
                        return redirect(url_for("staff_login"))
        except:
                return redirect(url_for("staff_login"))
        """
        available = request.form['available']
        quantity = request.form['quantity']
        try:
        	int(quantity)
        except:
        	flash("You must enter an amount")
        	return redirect(url_for('home'))
		
        if (int(quantity) > int(available)):
                flash("Silly")
        else:
                new_avail = int(available) - int(quantity)
                user = session["username"]
                barcode = request.form['barcode']
                price = request.form['price']
                db = sqlite3.connect('Database.db')
                cursor = db.cursor()
                cursor.execute('''INSERT INTO cart (meusername, barcode, quantity)
                                VALUES (?,?,?)''',(session['username'], barcode, quantity))

                # update quantity of stock
                cursor.execute('''UPDATE stock SET quantity = ? WHERE barcode = ? ''', (new_avail, barcode))
                db.commit()
                db.close()

        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        cursor.execute('''SELECT barcode, itemname, category, quantity,
                                 supplier, unitprice, saleflag, rating
                                 FROM stock WHERE saleflag = "true"''')
        all_rows = cursor.fetchall()
        db.close()

        all_items = []
        for row in all_rows:
                all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
                        'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'rating': row[7]})
        return redirect(url_for('home', items = all_items))


@app.route("/member/order-history")
def member_order_history():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	cursor.execute('''SELECT ordernum FROM rejectedOrders''')
	all_rows = cursor.fetchall()
	rejected = []
	for row in all_rows:
		rejected.append(row[0])
	
	cursor.execute('''SELECT ordernum FROM accepts''')
	all_rows = cursor.fetchall()
	accepted = []
	for row in all_rows:
		accepted.append(row[0])
	
	cursor.execute('''SELECT meusername, ordernum FROM checkout WHERE meusername=? ''', (session['username'],))
	all_rows = cursor.fetchall()
	
	all_items = []
	all_my_items = []
	for row in all_rows:
		order_accepted = "pending"
		ordernum = row[1]
		if ordernum in accepted:
			order_accepted = "true"
		elif ordernum in rejected:
			order_accepted = "false"
		
		cursor.execute('''SELECT barcode FROM contains WHERE ordernum = ?''', (ordernum,))
		all_barcodes = cursor.fetchall()
		try:
			barcode = all_barcodes[0][0]
		except:
			print "Error"
			barcode = 0
			
		cursor.execute('''SELECT itemname, supplier, rating FROM stock WHERE barcode = ? ''', (barcode,))
		all_items = cursor.fetchall()
		try:
			itemname = all_items[0][0]
			supplier = all_items[0][1]
			rating = all_items[0][2]
		except:
			print "error"
			itemname = ""
			supplier = ""
			rating = 0
			
		cursor.execute('''SELECT tprice, quantity FROM orders WHERE ordernum = ? ''', (ordernum,))
		all_orders = cursor.fetchall()
		try:
			price = all_orders[0][0]
			quantity = all_orders[0][1]
		except:
			print "error"
			price = 0
			quantity = 0
		
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (barcode,))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
			
		try:
			width = all_rows[0][1]
		except:
			width = 70
		
		all_my_items.append({'meusername': row[0], 'ordernum' : row[1], 'accepted': order_accepted, 'barcode': barcode,
							'itemname': itemname, 'price': price, 'supplier': supplier, 'quantity': quantity,
							'img_path': img_path, 'img_width': width})
	db.close()
	return render_template('member_order_history.html', items = all_my_items)
	
	
@app.route("/manager/hire-sp")
def hire_sp():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        cursor.execute('''SELECT name, applicantnum, phonenum, email, address, education, experience, extra FROM jobapplication''')
        all_rows = cursor.fetchall()
        db.close()
        
        all_apps = []
        for row in all_rows:
                all_apps.append({'name': row[0], 'app_num' : row[1], 'phone_num': row[2], 'email': row[3],\
                                                'address': row[4], 'education': row[5], 'experience': row[6], 'extra': row[7]})
                
        return render_template('hire_sp.html', apps=all_apps)
	
	
@app.route("/manager/hire-sp-update", methods=["POST"])
def hire_sp_update():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        app_num = request.form['app_num']
        accept = request.form['accept']
        
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        cursor.execute('''SELECT name, phonenum, email, address, education, experience,
                                                extra FROM jobapplication WHERE applicantnum=?''', (app_num,))
        all_rows = cursor.fetchall()
        new_name = ""
        for row in all_rows:
                new_name = row[0]
                phone_num = row[1]
                email = row[2]
                address = row[3]
                education = row[4]
                experience = row[5]
                extra = row[6]
                
        if not new_name:
                print "Error: application not found"
                return redirect(url_for('hire_sp'))
        
        cursor.execute('''SELECT username FROM user''')
        all_rows = cursor.fetchall()
        
        taken = True
        i = 0
        while(taken == True):
                i += 1
                taken = False
                temp_username = new_name + str(i)
                for row in all_rows:
                        if temp_username == row[0]:
                                taken = True
        
        
        temp_pwd = (''.join(random.choice(string.ascii_letters) for i in range(12)))
        print "username = " + temp_username
        temp_pwd = "password"
        
        if accept == "true":
                cursor.execute('''INSERT INTO user (username, password, name, email, position)
                VALUES (?,?,?,?,?)''', (temp_username, temp_pwd, new_name, email, "Salesperson"))
    
        cursor.execute('''DELETE FROM jobapplication WHERE applicantnum=? ''', (app_num,))
        db.commit()
        db.close()
        
        if accept == "true":
                flash('Job application accepted. New Salesperson added with username: "'+ \
                temp_username + '", and password: "' + temp_pwd + '" for Applicant: "' + new_name+'"')
        else:
                flash("Job application rejected.")
                
        return redirect(url_for('hire_sp'))
	
	
@app.route("/manager/view-sp")
def view_sp():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        # includes "fire sp" and "change salary"
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        cursor.execute('''SELECT username, password, name, email FROM user WHERE position="Salesperson" ''')
        all_rows = cursor.fetchall()
        
        salary = 0
        
        all_sps = []
        for row in all_rows:
        	cursor.execute('''SELECT salary FROM salesperson WHERE spusername=? ''', (row[0],))
        	all_rws = cursor.fetchall()
        	
        	try:
	        	salary = all_rws[0][0]
	        except:
	        	print "ERROR"
			
			
        	print "ROW " + str(all_rws)
        	all_sps.append({'username': row[0], 'password' : row[1], 'name': row[2], 'email': row[3], 'salary': salary})
        
			
        db.close()
        return render_template('view_sp.html', sps = all_sps)

@app.route("/manager/fire-sp", methods=["POST"])
def fire_sp():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        username = request.form['username']
        cursor.execute('''DELETE FROM user WHERE username = ? ''', (username,))
        
        flash("User " + username + " deleted")
        
        db.commit()
        return redirect(url_for('view_sp'))

	
	
@app.route("/manager/blacklist-member")
def blacklist_member():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        cursor.execute('''SELECT username, password, name, email, position FROM user 
                                                WHERE position="Member" ''')
        all_rows = cursor.fetchall()
        
        all_users = []
        
        for row in all_rows:
                
                cursor.execute('''SELECT isblist FROM member 
                                                WHERE meusername=? ''', (row[0],))
                temp_member = cursor.fetchone()
                print "OMG! " + str(temp_member)
                if temp_member and temp_member[0] == "true":
                        temp_member = True
                else:
                        temp_member = False
                
                all_users.append({'username':row[0], 'password': row[1], 'name': row[2], \
                                                                'email': row[3], 'position': row[4], 'blacklisted': temp_member})
        db.close()
        return render_template('blacklist_member.html', users=all_users)


@app.route("/manager/blacklist-update", methods=["POST"])
def blacklist_update():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        # doesnt work for sam1234
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        new_blist = request.form["new_blist"]
        username = request.form["username"]
        
        print "blist = " + new_blist + " username = " + username
        
        cursor.execute('''UPDATE member SET isblist = ? WHERE meusername = ? ''',
                (new_blist, username, ))

        db.commit()
        db.close()
        return redirect(url_for('blacklist_member'))


@app.route("/manager/search-items", methods=["POST"])
def mg_search_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	search_text = request.form["search"]

	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT barcode, itemname, category, quantity,
	                         supplier, unitprice, saleflag, rating
	                         FROM stock
	                         WHERE saleflag = "true"
                                       and quantity > 0
	                               		and (itemname LIKE '%'''+search_text+'''%'
	                               		or supplier LIKE '%'''+search_text+'''%'
	                               		or category LIKE '%'''+search_text+'''%') ''')
	all_rows = cursor.fetchall()

	all_items = []
	for row in all_rows:
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (row[0],))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
			
		try:
			width = all_rows[0][1]
		except:
			width = 70
		
		all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
		            'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'img_path': img_path, 'img_width': width,
		            'rating': row[7]})
	db.close()
	return render_template('mg_browse_items.html', items = all_items)


@app.route("/manager/change-salary", methods=["POST"])
def mg_change_salary():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	sm_username = request.form['username']
	new_salary = request.form['new_salary']

	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	cursor.execute('''UPDATE salesperson SET salary = ? WHERE spusername = ?''', (new_salary, sm_username))
	flash("Salary successfuly changed")
	                               		
	db.commit()
	db.close()
	return redirect(url_for('view_sp'))

	
@app.route("/manager/settings")
def mg_settings():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT username, password, name, email, position FROM user 
						WHERE position="Manager"
						AND username = ?''', (session['username'],))
						
	all_rows = cursor.fetchall()
	current_user = {}
	for row in all_rows:
		current_user = {'username':row[0], 'password': row[1], 'name': row[2], \
								'email': row[3], 'position': row[4]}
	
	db.close()
	
	return render_template('mg_settings.html', user = current_user)


@app.route("/manager/update_settings", methods=['POST'])
def mg_update_settings():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	username = request.form['username']
	name = request.form['name']
	password = request.form['password']
	email = request.form['email']
	
	
	if len(password) < 7:
		flash("Error: password must be at least 7 characters")
		return redirect(url_for('mg_settings'))
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''UPDATE user SET name = ?, password = ?, email = ?
													WHERE username = ?
													AND position="Manager" ''',
											(name, password, email, username,))
											
	session['currName'] = name
	
	db.commit()
	db.close()
	flash("Settings updated")
	return redirect(url_for('mg_settings'))
	
	
@app.route("/director/promote-demote")
def promote_demote():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        cursor.execute('''SELECT username, password, name, email, position FROM user 
                                                WHERE position="Manager" OR position="Salesperson" ''')
        all_rows = cursor.fetchall()
        
        current_user = []
        
        for row in all_rows:
                current_user.append({'username':row[0], 'password': row[1], 'name': row[2], \
                                                                'email': row[3], 'position': row[4]})
        db.close()
        
        return render_template('promote_demote.html', users = current_user)
	
	
@app.route("/director/promote-update", methods=['POST'])
def promote_update():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        username = request.form['username']
        action = request.form['action']
        
        if action == "promote":
                cursor.execute('''UPDATE user SET position = "Manager" WHERE username = ? ''', (username,))
                flash(username + " Promoted to Manager")
        else:
                cursor.execute('''UPDATE user SET position = "Salesperson" WHERE username = ? ''', (username,))
                flash(username + " Demoted to Salesperson")
        db.commit()
        db.close()
        
                
        return redirect(url_for('promote_demote'))


@app.route("/director/search-items", methods=["POST"])
def dt_search_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	search_text = request.form["search"]

	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT barcode, itemname, category, quantity,
	                         supplier, unitprice, saleflag, rating
	                         FROM stock
	                         WHERE saleflag = "true"
                                       and quantity > 0
	                               		and (itemname LIKE '%'''+search_text+'''%'
	                               		or supplier LIKE '%'''+search_text+'''%'
	                               		or category LIKE '%'''+search_text+'''%') ''')
	all_rows = cursor.fetchall()

	all_items = []
	for row in all_rows:
		cursor.execute('''SELECT path, width FROM pictures WHERE barcode = ? ''', (row[0],))
		all_rows = cursor.fetchall()
		
		try:
			img_path = all_rows[0][0]
		except:
			img_path = 'images/no_img.svg'
			
		try:
			width = all_rows[0][1]
		except:
			width = 70
		
		all_items.append({'barcode': row[0], 'itemname' : row[1], 'category': row[2], 'quantity': row[3], \
		            'supplier': row[4], 'price': row[5], 'saleflag': row[6], 'img_path': img_path, 'img_width': width,
		            'rating': rating})
	db.close()
	return render_template('dt_browse_items.html', items = all_items)


@app.route("/director/settings")
def dt_settings():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''SELECT username, password, name, email, position FROM user 
						WHERE position="Director"
						AND username = ?''', (session['username'],))
						
	all_rows = cursor.fetchall()
	current_user = {}
	for row in all_rows:
		current_user = {'username':row[0], 'password': row[1], 'name': row[2], \
								'email': row[3], 'position': row[4]}
	
	db.close()
	
	return render_template('dt_settings.html', user = current_user)


@app.route("/director/update_settings", methods=['POST'])
def dt_update_settings():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	
	username = request.form['username']
	name = request.form['name']
	password = request.form['password']
	email = request.form['email']
	
	if len(password) < 7:
		flash("Error: password must be at least 7 characters")
		return redirect(url_for('dt_settings'))
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	cursor.execute('''UPDATE user SET name = ?, password = ?, email = ?
													WHERE username = ?
													AND position="Director" ''',
											(name, password, email, username,))
											
	session['currName'] = name
	
	db.commit()
	db.close()
	flash("Settings updated")
	return redirect(url_for('dt_settings'))

	
@app.route("/director/add-items", methods=['POST'])
def dt_add_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        itemname = request.form['itemname']
        price = request.form['price']
        category = request.form['category']
        quantity = request.form['quantity']
        supplier = request.form['supplier']
        
        try:
                onsale = request.form['onsale']
        except:
                onsale = "false"
        
        cursor.execute('''INSERT INTO stock(itemname, unitprice, category, quantity, supplier, saleflag)
                VALUES(?,?,?,?,?,?)''', (itemname, price, category, quantity, supplier, onsale))
    
        db.commit()
        db.close()
        return redirect(url_for('dt_browse_items'))


@app.route("/director/remove-items", methods=['POST'])
def dt_remove_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        barcode = request.form['barcode']
        cursor.execute('''DELETE FROM stock WHERE barcode = ? ''', (barcode,))
        
        db.commit()
        db.close()
        return redirect(url_for('dt_browse_items'))


@app.route("/director/sale-flag", methods=['POST'])
def dt_edit_items():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
        db = sqlite3.connect('Database.db')
        cursor = db.cursor()
        
        barcode = request.form['barcode']
        
        try:
                saleflag = request.form['saleflag']
                cursor.execute('''UPDATE stock SET saleflag = ? WHERE barcode = ? ''',
                                                                                        (saleflag, barcode))
        except:
                ""
        
        
        
        db.commit()
        db.close()
        return redirect(url_for('dt_browse_items'))


@app.route("/apply")
def apply():
	return render_template('apply.html')




@app.route("/submit-app", methods=["POST"])
def submit_app():
	
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	name = request.form['name']
	
	phone_number = request.form['num']	
	email = request.form['email']	
	address = request.form['address']	
	education = request.form['education']	
	experience = request.form['experience']	
	extra = request.form['extra']
	
	cursor.execute('''SELECT email FROM user''')
	all_rows = cursor.fetchall()
	taken = False
	for row in all_rows:
		if email == row[0]:
			taken = True
			message = "Error: Another user using is using that email address"
	
	cursor.execute('''SELECT email, phonenum, address FROM jobapplication''')
	all_rows = cursor.fetchall()
	for row in all_rows:
		if email == row[0]:
			taken = True
			message = "Error: Another user using is using that email address"
		if phone_number == row[1]:
			taken = True
			message = "Error: Another user using is using that phone number"
		if address == row[2]:
			taken = True
			message = "Error: Another user using is using that address"
		
	if taken:
		return render_template('submit_app.html', message=message)
	
	cursor.execute('''INSERT INTO Jobapplication (name, phonenum, email, address, education, experience, extra)
                  VALUES (?,?,?,?,?,?,?)''', (name, phone_number, email, address, education, experience, extra))
    
	db.commit()
	db.close()
	
	message = "Success! "+name+", your application has been submitted"
	return render_template('submit_app.html', message=message)


@app.route("/member/rate", methods=['POST'])
def rate_product():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	rating = request.form['rating']
	barcode = request.form['barcode']
	   
	cursor.execute('''UPDATE stock SET rating = ? WHERE barcode = ?''', (rating, barcode))
	
	flash("Thank you for rating")
	
	db.commit()
	db.close()
	return redirect(url_for('member_order_history'))


@app.route("/member/comment", methods=['POST'])
def comment_on_product():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	comment = request.form['comment']
	barcode = request.form['barcode']
	
	cursor.execute('''INSERT INTO comment(barcode, str, date) VALUES(?,?,?) ''', (barcode, comment, time.strftime("%d/%m/%Y")))
	
	flash("Thank you for commenting")
	
	db.commit()
	db.close()
	return redirect(url_for('member_order_history'))


@app.route("/view-comments", methods=['POST'])
def view_comments():
	"""
	try:
		if not session['logged_in']:
			return redirect(url_for("staff_login"))
	except:
		return redirect(url_for("staff_login"))
	"""
	db = sqlite3.connect('Database.db')
	cursor = db.cursor()
	
	barcode = request.form['barcode']
	itemname = request.form['itemname']
	
	cursor.execute('''SELECT str FROM comment WHERE barcode = ?''', (barcode,))
	all_rows = cursor.fetchall()
	
	all_comments = []
	for row in all_rows:
		all_comments.append({'barcode': barcode, 'itemname': itemname, 'comment': row[0]})
	
	db.commit()
	db.close()
	return render_template('view_comments.html', comments = all_comments)
	
	
if __name__ == "__main__":
    app.run()








